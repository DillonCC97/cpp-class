// This is a comment

/*
  This is a block comment
*/

#include <iostream> // input output stream for printing
#include <vector> // allows use of vectors
#include <string> //allows use of strings
#include <fstream> //allows input output of files

using namespace std; // most common namespace

int main(){ //main function that runs, output integer
  
  cout << "Hello World" << endl; // prints string to one line

  const double PI = 3.1415926535; // constants cant be changes later, double is floated number
  char myGrade = 'A'; // chars include single character
  bool isHappy = true; // booleans can be true or false
  int myAge = 19; // integers are any whole number within size limit
  float favNum = 4.12131; // accurate up to 6 decimal points usually
  double otherFavNum = 1.23465343543456; // accurate up to about 15 decimal points usually

  cout << "Favorite Number " << favNum << endl; // you can send multiple values to cout

  /* Other Data Types:
     short int : at least 16 bits
     long int : at least 32 bits
     long long int : at least 64 bits
     unsigned int : same size as signed version
     long double : not less than double in size
  */

  cout << "Size of int " << sizeof(myAge) << endl; // sizeof checks data size
  cout << "Size of char " << sizeof(myGrade) << endl;
  cout << "Size of bool " << sizeof(isHappy) << endl;
  cout << "Size of float " << sizeof(favNum) << endl;
  cout << "Size of double " << sizeof(otherFavNum) << endl;

  int largestInt = 2147483647; // largest possible integer
  int tooLargeInt = 2147483648; // if the integer is too large it will wrap around and become negative

  cout << "Largest possible int: " << largestInt << endl;
  cout << "Int that is too large: " << tooLargeInt << endl;

  // Arithmetic operators: +, -, *, /, %
  cout << "5 + 2 = " << 5 + 2 << endl;
  cout << "5 - 2 = " << 5 - 2 << endl;
  cout << "5 * 2 = " << 5 * 2 << endl;
  cout << "5 / 2 = " << 5 / 2 << endl; // no decimal points since int, see casting
  cout << "5 % 2 = " << 5 % 2 << endl;

  // Incrementing operators: ++, --, +=, -=
  int five = 5;

  cout << "5++ = " << five++ << endl; // five prints then increments by one
  cout << "++5 = " << ++five << endl; // five increments by one then prints
  cout << "5-- = " << five-- << endl; // five prints then decreases by one
  cout << "--5 = " << --five << endl; // five decreases by one then prints
  cout << "5 += 6 = " << (five += 6) << endl; // five's value is increased by 6
  cout << "5 -= 6 = " << (five -= 6) << endl; // five's value is decreased by 6

  // Order of operations: * and / performed first then left to right
  cout << "1 + 2 - 3 * 2 = " << 1 + 2 - 3 * 2 << endl;
  cout << "(1 + 2 - 3) * 2 = " << (1 + 2 - 3) * 2 << endl;

  // Casting types
  cout << "4 / 5 = " << 4 / 5 << endl; // result returned as integer so no decimal points
  cout << "4 / 5 = " << (double) 4 / 5 << endl; // result casted to double type, so decimals show

  // Comparison Operators: == (equivalent), != (not equivalent), < (less than), > (greater than), <= (less than or equal to), >= (greater than or equal to)
  // Logical Operators: && (and), || (or), ! (not)

  // Logic Control: if statements
  int age = 70;
  int ageAtLastExam = 16;
  bool isNotIntoxicated = true;

  if ((age >= 1) && (age < 16)){ // if(condition){  }
    cout << "You can't drive!" << endl;
  } else if(! isNotIntoxicated){
    cout << "You can't drive!" << endl;
  } else if(age >= 80 && ((age > 100) || ((age - ageAtLastExam) > 5))){
    cout << "You can't drive!" << endl;
  } else {
    cout << "You can drive!" << endl;
  }
  // Sequencing Operator: ',' denotes functions will be performed left to right
  int a;
  int b;
  a = (b=2, b+2);
  cout << "Variable 'a' should equal 4: " << a << endl;
  
  // Logic Control: switch statements
  int greetingOption = 2;

  switch(greetingOption){
  case 1:
    cout << "bonjour" << endl;
    break; // include break to prevent other cases from running
  case 2:
    cout << "Hola" << endl;
    break;
  case 3:
    cout << "Hallo" << endl;
    break;
  default: // will trigger if input does not match a defined case
    cout << "Hello" << endl;
  }

  // Logic Control: conditionals
  // variable = (condition) ? true : false
  int largestNum = (5>2) ? 5 : 2;
  cout << largestNum << endl;

  // Arrays
  int myFavNums[5]; // creates array of size 5
  int badNums[5] = {4, 13, 14, 24, 34};

  cout << "Bad Number 1: " << badNums[0] << endl; // prints value at index 0

  char myName[2][4] = {{'D', 'i', 'l', 'l'}, // creates 2 row 4 column array
                       {'C', 'u', 'l', 'p'}};

  cout << "Forth Letter in Second Array: " << myName[1][3] << endl; // prints value in first row 3rd column
  
  myName[0][3] = 'n'; // indexes in arrays can be modified freely
  cout << "New Value: " << myName[0][3] << endl;

  // Flow Control: for loop
  for(int i = 1; i <= 10; i++){ // for( iterating_variable; conditional; iteration){
    cout << i << " ";
  }
  cout << endl;
  
  for(int j = 0; j < 2; j++){ // for loops can be nested as expected
    for(int k = 0; k < 4; k++){
      cout << myName[j][k];
    }
  }
  cout << endl;

  // Flow Control: while loop
  int randNum = (rand() % 100) + 1; // random number from 1 - 100

  while(randNum != 100){ // while(condition){
    cout << randNum << ", ";
    randNum = (rand() % 100) + 1;
  }
  cout << endl;

  // Flow Control: Do while loops
  string numberGuessed;
  int intNumberGuessed = 0;

  do{ // do{ } while(condition)
    cout << "Guess number between 1 and 10: ";
    getline(cin, numberGuessed); // take line of input from user
    intNumberGuessed = stoi(numberGuessed);
    cout << intNumberGuessed << endl;
  } while(intNumberGuessed != 4);
  cout << "You Win!" << endl;

  // Strings
  string birthdayString = "birthday";
  string happyString = "Happy ";
  cout << happyString + birthdayString << endl; // strings can be added as expected

  string yourName;
  cout << "What is your name: ";
  getline(cin, yourName); // prompt user and ask for input

  cout << "Hello " << yourName << endl;

  double eulersConstant = .57721;
  string eulerGuess;
  double eulerGuessDouble;

  cout << "What is Euler's Constant? ";
  getline(cin, eulerGuess);

  eulerGuessDouble = stod(eulerGuess);

  if(eulerGuessDouble == eulersConstant){
    cout << "You are right" << endl;
  } else {
    cout << "You are wrong" << endl;
  }

  // String operators
  cout << "Size of String " << eulerGuess.size() << endl; // checks size of string
  cout << "Is string empty " << eulerGuess.empty() << endl; // returns true is string is empty
  cout << eulerGuess.append(" is your guess of Euler's constant") << endl; // appends on end of string

  string dogString = "dog";
  string catString = "cat";

  cout << dogString.compare(catString) << endl; // returns 1 because of alphabetical order
  cout << dogString.compare(dogString) << endl; // returns 0 because strings are equivalent
  cout << catString.compare(dogString) << endl; // returns -1 because of alphabetical order

  string fullName = "Dillon Culp";
  cout << fullName << endl;
  string myFullName = fullName.assign(fullName);
  string firstName = myFullName.assign(myFullName, 0, 6); // assign pulls a substring between indexes
  cout << firstName << endl;

  cout << "Full Name: " << fullName << endl;
  int lastNameIndex = fullName.find("Culp", 0);
  cout << "Index for last name " << lastNameIndex << endl;

  fullName.insert(6, " Charles");
  cout << fullName << endl;

  fullName.erase(5, 7);
  cout << fullName << endl;

  vector <int> lotteryNumVect(10); // initially size 10 vectors can change size
  int lotteryNumArray[5] = {4, 13, 14, 24, 34};

  lotteryNumVect.insert(lotteryNumVect.begin(), lotteryNumArray, lotteryNumArray + 3); // insert first three numbers from array to beginning of vector
  cout << lotteryNumVect.at(2) << endl; // print value at index 2

  lotteryNumVect.insert(lotteryNumVect.begin() + 5, 44); // insert 44 at the 5th index from the beginning
  cout << lotteryNumVect.at(5) << endl;

  lotteryNumVect.push_back(64); // inserts value at the end of vector
  cout << "First Value " << lotteryNumVect.front() << endl; // shows first value in vector
  cout << "Last Value " << lotteryNumVect.back() << endl; // shows last value in vector
  lotteryNumVect.pop_back(); // removes final value in vector
  cout << "New Last Value "  << lotteryNumVect.back() << endl;
  cout << "Is vector empty? " << lotteryNumVect.empty() << endl;
  cout << "Size of Vector: " << lotteryNumVect.size() << endl;


  // Input/Output Control
  /* cin = standard input control
     cout = standard output control
     cerr = standard error output control
     clog = standard log output control
  */
  int ageInputInt;
  string ageInput;
  cout << "What is your age? ";
  // cin >> ageInputInt; this works but is bad practice, don't mix >> with getline()
  getline(cin, ageInput);
  ageInputInt = stoi(ageInput);
  cout << "Age input: " << ageInputInt << endl;

  // Whitespace terminates cin, so use getline() to take in strings that will have spaces
  string nameInput;
  cout << "What is your name? ";
  getline(cin, nameInput);
  cout << "Name Input: " << nameInput << endl;

  // TODO - Jump Statements(continue, goto)
}

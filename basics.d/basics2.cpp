#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

// Functions //
int addNumbers(int firstNum, int secondNum = 0){ // second number has default value of 0, if no value is passed as secondNum then 0 is passed)
  int combinedValue = firstNum + secondNum;
  return combinedValue;
}

int addNumbers(int firstNum, int secondNum, int thirdNum){ // function overloading works as expected
  return firstNum + secondNum + thirdNum;
}

int getFactorial(int number){ 
  int sum;
  if(number ==1) sum = 1;
  else sum = getFactorial(number - 1) * number; // recursive functions work as expected
  return sum;
}

int main(){
  cout << addNumbers(1,2) << endl; // calling functions works as expected
  cout << addNumbers(1,2,3) << endl;
  cout << "Factorial of 3 is: " << getFactorial(3) << endl;

  string printQuote = "Kill People, Break Shit, Fuck School";
  ofstream writer("motto.txt"); // create writer to write to "motto.txt"

  if(! writer){ // check to see if file can be opened
    cout << "Error opening file" << endl;
    return -1; // if file cant be opened return error message and terminate with -1
  } else {
    writer << printQuote << endl;
    writer.close(); // close writer to save data
  }

  ofstream writer2("motto.txt", ios::app);

  /* ios::app append to what is already in file
     ios::binary treat file as binary
     ios::in Open a file to read input
     ios::trunc Default
     ios::out Open a file to write output*/

  if(! writer2){
    cout << "Error opening file" << endl;
    return -1;
  } else {
    writer2 << endl << "-Tyler The Creator" << endl;
    writer2.close();
  }

  char letter;
  ifstream reader("motto.txt");

  if(! reader){
    cout << "Error opening file" << endl;
    return -1;
  } else {
    for(int i = 0; ! reader.eof(); i++){
      reader.get(letter);
      cout << letter;
    }
    cout << endl;
    reader.close();
  }
}

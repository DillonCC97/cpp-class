#include<iostream>
#include<iomanip>
#include<limits>

#define MAX_SIZE 15

class Box {
public:
  void setSize(int input){
    size = input;
  }
  int getSize(){
    return size;
  }
  void setBox(){
    int row = 0, col = size/2, i, square = size*size;
    for(i = 1; i <= square; i++) {
      magicBox[row][col] = i;
      if(i % size == 0) row++;
      else {
        if(row == 0) row = size - 1;
        else row--;
        if(col == (size - 1)) col = 0;
        else col++;
      }
    }
    boxNum = 1;
  }
  void printBox(){
    std::cout <<"Magic Square #" << boxNum << " for n = " << size << "\n\n";
    for(int i = 0; i < size; i++){
      for(int j = 0; j < size; j++){
        std::cout << std::setw(4) << magicBox[i][j];
      }
      std::cout << "\n";
    }
    std::cout << "\n";
  }
  void checkBox(){
    int diagonalSum1 = 0;
    int diagonalSum2 = 0;
    int rowSums[size];
    int colSums[size];
    std::fill_n(rowSums, size, 0);
    std::fill_n(colSums, size, 0);
    
    for(int l = 0; l < size; l++){
      for(int m = 0; m < size; m++){
        if (l == m){
          diagonalSum1 += magicBox[l][m];
        }
        if((size - 1) - l == m){
          diagonalSum2 += magicBox[l][m];
        }
        rowSums[l] += magicBox[l][m];
        colSums[m] += magicBox[l][m];
      }
    }
    
    std::cout << "Checking the sums of every row: ";
    for(int i = 0; i < size; i++){
      std::cout << rowSums[i] << " ";
    }
    std::cout << "\n";
    std::cout << "Checking the sums of every column: ";
    for(int i = 0; i < size; i++){
      std::cout << colSums[i] << " ";
    }
    std::cout << "\n";
    std::cout << "Checking the sums of every diagonal: " << diagonalSum1 << " " << diagonalSum2 << "\n\n";
  }

  void rotateBox(){
    int tmp;
    for(int i = 0; i < size / 2; i++){
      for(int j = i; j < size - i - 1; j++){
        tmp = magicBox[i][j];
        magicBox[i][j] = magicBox[j][size-i - 1];
        magicBox[j][size - i - 1] = magicBox[size - i - 1][size - j - 1];
        magicBox[size - i - 1][size - j - 1] = magicBox[size - j - 1][i];
        magicBox[size - j - 1][i] = tmp;
      }
    }
  }

protected:
  int size;
  int magicBox[MAX_SIZE][MAX_SIZE];
  int boxNum;
};

int main(){
  int n;
  double input;
  while(true){
    std::cout << "Enter the size of the magic square: ";
    std::cin >> input;
    if(std::cin.fail()){
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      std::cout << "Input is not a number!\n";
      continue;
    } else if(input != (int)input){
      std::cout << "Input is not a whole number\n";
      continue;
    }
    n = input;
    if(n < 1){
      std::cout << "Input is not a positive number!\n";
      std::cin.clear();
      continue;
    } else if(n > 15){
      std::cerr << "Requested magic box is too large!" << "\n";
      std::cin.clear();
      continue;
    } else if(n % 2 == 0){
      std::cout << "Requested magic box does not have odd dimensions!\n";
      std::cin.clear();
      continue;
    } else {
      Box magicBox;
      magicBox.setSize(n);
      magicBox.setBox();
      magicBox.printBox();
      magicBox.checkBox();
      magicBox.rotateBox();
      magicBox.printBox();
      magicBox.checkBox();
      magicBox.rotateBox();
      magicBox.printBox();
      magicBox.checkBox();
    }
    return 0;
  }
}

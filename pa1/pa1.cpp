/*
  pa1
  Written by Dillon Culp
*/
#include "pa1.h"
#include<iostream>
#include<iomanip>
#include<limits>

#define MAX_SIZE 15

int main() {
  int n;
  double input;
  // Take user input
  while(true){
    std::cout << "Enter the size of the magic square: ";
    std::cin >> input;
    if(std::cin.fail()){
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      std::cout << "Input is not a number!\n";
      continue;
    } else if(input != (int)input){
      std::cout << "Input is not a whole number\n";
      continue;
    }
    // Prevent user from entering a size value too large to compute
    n = input;
    if(n < 1){
      std::cout << "Input is not a positive number!\n";
      std::cin.clear();
      continue;
    } else if(n > 15){
      std::cerr << "Requested magic box is too large!" << "\n";
      std::cin.clear();
      continue;
    } else if(n % 2 == 0){
      std::cout << "Requested magic box does not have odd dimensions!\n";
      std::cin.clear();
      continue;
    } else {
      generateSquare1(n);
    }
    return 0;
  }
}


void generateSquare1(int size) {
  
  int row = 0, col = size/2, i, j, square = size*size;

  int magicBox[MAX_SIZE][MAX_SIZE];
  /*
    Start with the middle of the top row as 1
    Iterate by 1 going down and to the left until
    you either reach an edge of the array or you
    count to a factor of the size
  */
  for(i = 1; i <= square; i++) {
    magicBox[row][col] = i;
    // If current index is a factor of the array's size, go down a row
    if(i % size == 0) row++;
    else {
      // If the current index is at the first row, go to the last row
      if(row == 0) row = size -1;
      // If the current index is not at the first row, go to the previous row
      else row--;
      // If the current index is at the last column, go to the first column
      if(col == (size - 1)) col = 0;
      // If the current index is not at the last column, go to the next column
      else col++;
    }
  }

  std::cout << "The Magic Squares for n = " << size << ":\n\n";

  // Rotate the array twice and print all the information for each magic square
  for(int twice = 0; twice <= 2; twice++){
    int diagonalSum1 = 0;
    int diagonalSum2 = 0;
    int rowSums[size];
    int colSums[size];
    std::fill_n(rowSums, size, 0);
    std::fill_n(colSums, size, 0);

    // Print values in 2d magic box array
    std::cout << "Magic Square #" << twice+1 <<  " is:\n\n";
    for(i=0; i<size; i++) {
      for(j=0; j<size; j++) 
        std::cout << std::setw(4) << magicBox[i][j];
      std::cout << "\n";
    }
    std::cout << "\n";

    // Check sums of each magic box
    for(int l = 0; l < size; l++){
      for(int m = 0; m < size; m++){

        // Find the sum of values on the NW to SE diagonal
        if(l == m){
          diagonalSum1 += magicBox[l][m];
        }
        // Find the values of sums on the NE to SW diagonal
        if((size - 1) - l == m){
          diagonalSum2 += magicBox[l][m];
        }
        // Add values to the index of an array for every row and every column
        rowSums[l] += magicBox[l][m];
        colSums[m] += magicBox[l][m];
      }
    }

    // Print info on magic box
    std::cout << "Checking the sums of every row: ";
    for(int i = 0; i < size; i++){
      std::cout << rowSums[i] << " ";
    }
    std::cout << "\n";
    std::cout << "Checking the sums of every column: ";
    for(int i = 0; i < size; i++){
      std::cout << colSums[i] << " ";
    }
    std::cout << "\n";
    std::cout << "Checking the sums of every diagonal: " << diagonalSum1 << " " << diagonalSum2 << "\n\n";
  

    // Rotate magic box
    int tmp;
    for(int i = 0; i < size / 2; i++){
      for(int j = i; j < size - i - 1; j++){
        tmp = magicBox[i][j];
        magicBox[i][j] = magicBox[j][size-i - 1];
        magicBox[j][size - i - 1] = magicBox[size - i - 1][size - j - 1];
        magicBox[size - i - 1][size - j - 1] = magicBox[size - j - 1][i];
        magicBox[size - j - 1][i] = tmp;
      }
    }
  }
}

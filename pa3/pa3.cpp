/*
  pa3
  Written by Dillon Culp
*/

#include<iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

const int MAX_STACK = 2000;

class stack {
private:
  string name[MAX_STACK];
  int top;
public:
  stack(){
    top = -1;
  }

  void push(string element){
    name[++top] = element;
  }

  string pop() {
    return name[top--];
  }

  bool empty() {
    if(top == -1){
      return true;
    } else {
      return false;
    }
  }
};

vector<string> split(string str, char delimiter) {
  vector<string> internal;
  stringstream ss(str); // Turn the string into a stream.
  string tok;
  
  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
  
  return internal;
}

int read(string filename) {
  stack keywords;
  stack identifiers;
  stack constants;
  stack operators;
  stack delimiters;
  stack errors;
  int depth = 0;
  int maxdepth = 0;
  ifstream file(filename);
  string str;
  for(string line; getline(file, line ); ) {
    vector<string> linevec = split(line, ' ');

    bool iskeyword = true;
    for(unsigned int i=0; i < linevec[0].length(); i++){
      if(islower(linevec[0][i])){
        iskeyword = false;
      }
    }
    if(iskeyword) {
      if(linevec[0] == "BEGIN") {
        //push BEGIN to keywords stack
        keywords.push("BEGIN");
        depth++;
        maxdepth++;
      } else if (linevec[0] == "FOR") {
        //push FOR to keyword stack
        keywords.push("FOR");
        for(unsigned int i = 0; i < linevec.size(); i++){
          string arg = linevec[1];
          if(arg[0] == '(') {
            arg.erase(0, 1);
          }
          if (arg[arg.size() - 1] == ',') {
            arg.erase(arg.size() - 1);
            // push comma to delimeter stack
            delimiters.push(",");
          }
          if (arg[arg.size() - 1] == ')') {
            arg.erase(arg.size() - 1);
          }
          bool isNumber = true;
          bool isWord = true;
          
          for(unsigned int j = 0; j < arg.size(); j++){
            if(!(isdigit(arg[j]))){
              isNumber = false;
            }
            if(!(isalpha(arg[j]))){
              isWord = false;
            }
          }

          if(isNumber){
            //push to constant stack
            constants.push(arg);
          } else if (isWord) {
            //push to identifier stack
            identifiers.push(arg);
          } else {
            //push to Operator stack
            operators.push(arg);
          }
        }
      }else if (linevec[0] == "END"){
        //push END to keywords stack
        keywords.push("END");
        depth--;
      } else {
        size_t equalind = linevec[0].find('=');
        if(equalind != string::npos){
          string identifier = linevec[0].substr(0, equalind);
          //push identifier to stack
          identifiers.push(identifier);
          string arg = linevec[0].substr(equalind, linevec[0].size());
          // push
          bool isNumber = true;
          bool isWord = true;
          
          for(int k = 0; k < arg.size(); k++){
            if(!(isdigit(arg[k]))){
              isNumber = false;
            }
            if(!(isalpha(arg[k]))){
              isWord = false;
            }
          }

          if(isNumber){
            //push to constant stack
            constants.push(arg);
          } else if (isWord) {
            //push to identifier stack
            identifiers.push(arg);
          } else {
            //push to Operator stack
            operators.push(arg);
          }
        }

        for(unsigned int i = 1; i < linevec.size(); i++){
          string arg = linevec[1];
          if(arg[0] == '(') {
            arg.erase(0, 1);
          }
          if (arg[arg.size() - 1] == ',') {
            arg.erase(arg.size() - 1);
            // push comma to delimeter stack
            delimiters.push(",");
          }
          if (arg[arg.size() - 1] == ')') {
            arg.erase(arg.size() - 1);
          }
          if (arg[arg.size() - 1] == ';') {
            arg.erase(arg.size() - 1);
            //push semicolon to delimetr stack
            delimiters.push(";");
          }
          bool isNumber = true;
          bool isWord = true;
          
          for(i = 0; i < arg.size(); i++){
            if(!(isdigit(arg[i]))){
              isNumber = false;
            }
            if(!(isalpha(arg[i]))){
              isWord = false;
            }
          }

          if(isNumber){
            //push to constant stack
            constants.push(arg);
          } else if (isWord) {
            //push to identifier stack
            identifiers.push(arg);
          } else {
            //push to Operator stack
            operators.push(arg);
          }
        }
        if (depth < 0){
          //push syntax error to stack
          errors.push("Superfluous END");
        }
      }
      if(depth != 0){
        errors.push("Missing END");
      }
    }
  }
  cout << "The depth of nested loop(s) is " << maxdepth << "\n\n";
  cout << "Keywords: ";
  while(!keywords.empty()) {
    cout << keywords.pop();
  }
  cout << "\nIdentifier: ";
  while(!identifiers.empty()) {
    cout << identifiers.pop();
  }
  cout << "\nConstant: ";
  while(!constants.empty()) {
    cout << constants.pop();
  }
  cout << "\nOperator: ";
  while(!operators.empty()) {
    cout << operators.pop();
  }
  cout << "\nDelimiter: ";
  while(!delimiters.empty()) {
    cout << delimiters.pop();
  }
  cout << "\n\nSyntax Error(s): ";
  while(!errors.empty()) {
    cout << errors.pop();
  }
  return 0;
}

int main() {
  string input;
  cout << "Please enter the name of the input file: ";
  cin >> input;
  read(input);
  return 0;
}

const int MaxStack = 10000; 
const char EmptyFlag = '\0'; 

class Stack { 

    char items[MaxStack]; 
    int top; 
public: 
    enum { FullStack = MaxStack, EmptyStack = -1 }; 
    enum { False = 0, True = 1}; 
    // methods 
    void init(); 
    void push(char); 
    char pop(); 
    int empty(); 
    int full(); 
    void dump_stack(); 
};

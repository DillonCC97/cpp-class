#include <iostream>
#include <string>

using namespace std;

int main(){

  //take input
  string ufID;
  cout << "Input your UFID: ";
  getline(cin, ufID);

  //store into array
  int ufID_array[ufID.size()];
  int i = 0;
  for(char& c : ufID){

    ufID_array[i] = c - '0';
    i++;

  }

  //get sum of digits less than or equal to first digit
  int first = ufID_array[0];
  int run_num = 0;
  int less_sum = 0;
  int sum = 0;
  for(int& i : ufID_array){
    if(run_num != 0){
      if(i > first){
        cout << i << ", ";
        less_sum += i;
      }
      sum += i;
    }
    run_num++;
  }
  cout << "are less than or equal to the first digit " << first << endl;

  //get sum of other digits
  cout << "Sum of other digits: " << sum - less_sum << endl;
}

#include <iostream>
#include <string>

using namespace std;

int main(){
  //input into array
  string user_input;
  cout << "Input 10 integer string separated by space: ";
  int user_array[10] = {};
  for(int i = 0; i < 10; i++){
    cin >> user_array[i];
  }

  //search for digit
  int search;
  cout << "Search for: ";
  cin >> search;
  for(int i = 0; i < 10; i++){
    if(user_array[i] == search){
      for(int j = i; j < 10; j++){
        user_array[j] = user_array[j+1];
      }
      user_array[9] = 0;
      break;
    }
  }

  //print array
  cout << "Output Array: ";
  for(int& c : user_array){
    cout << c << " ";
  }
  cout << endl;

}
  
  

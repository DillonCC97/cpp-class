#include <iostream>
#include <cstdlib>
#include <string>
#include <curses.h>
#define MAPSIZEY 20
#define MAPSIZEX 30

using namespace std;

class Entity {
private:
  char avatar;
  int health;
  int posX;
  int posY;
public:
  int deltay = 0, deltax = 0, hp = 100;
  void init(int newy, int newx, int newhp, char newavatar) {
    setpos(newx, newy);
    sethealth(newhp);
    setavatar(newavatar);
  }

  void setpos(int x, int y){
    posX = x;
    posY = y;
  }

  int getposX(){
    return posX;
  }

  int getposY(){
    return posY;
  }

  void sethealth(int x){
    health = x;
  }

  int gethealth(){
    return health;
  }

  void setavatar(char x){
    avatar = x;
  }

  char getavatar(){
    return avatar;
  }

  void drawplayer(char (&map)[MAPSIZEY][MAPSIZEX]) {
    map[getposY()][getposX()] = getavatar();
  }

  void move(int deltay, int deltax) {
    if(mvinch(getposY()+deltay,getposX()+deltax) =='.' || mvinch(getposY() + deltay, getposX() + deltax) == '#'){
      setpos(getposX() + deltax, getposY() + deltay);
    }
  }

  void printstats(int row) {
    mvprintw(row, 0, "HP: %3d", hp);
    mvprintw(row+1, 0, "x: %3d", getposX());
    mvprintw(row+2, 0, "y: %3d", getposY());
  }
};

class Screen {
private:
public:
  char map[MAPSIZEY][MAPSIZEX];
  void initmap() {
    for(int mapy = 0; mapy <= MAPSIZEY; mapy++) {
      for( int mapx = 0; mapx <=MAPSIZEX; mapx++) {
        if(mapy == 0 || mapx == 0 || mapx == MAPSIZEX || mapy == MAPSIZEY) {
          map[mapy][mapx] = '+';
        } else {
          map[mapy][mapx] = '.';
        }
      }
    }
  }
  void drawmap() {
    for(int mapy = 0; mapy <= MAPSIZEY; mapy++) {
      for( int mapx = 0; mapx <=MAPSIZEX; mapx++) {
        mvaddch(mapy, mapx, map[mapy][mapx]);
      }
    }
  }
};
  
int main() {
  initscr();
  raw();
  curs_set(0);
  noecho();
  Screen screen;
  screen.initmap();
  Entity player;
  player.init(1,1,100, '@');
  char key;
  int deltax = 0, deltay = 0;
  do {
    switch (key) {
    case 'q': {
      deltay=-1; deltax=-1;
      break;
    }
    case 'w': {
      deltay=-1; deltax=0;
      break;
    }
    case 'a': {
      deltay=0; deltax=-1;
      break;
    }
    case 's': {
      deltay=1; deltax=0;
      break;
    }
    case 'd': {
      deltay=0; deltax=1;
      break;
    }
    }
    player.move(deltay,deltax);
    screen.initmap();
    player.drawplayer(screen.map);
    screen.drawmap();
    screen.drawmap();
    player.printstats(24);
  }
  while(27 != (key = getch()));
  refresh();
  endwin();
  return 0;
}

#include <iostream>
#include <string>
#include <cmath>
#include <algorithm>
#include <unistd.h>

using namespace std;

void checkPowerOfTwo(){

  string numberCheck;
  cout << "Type number to check if power of 2: ";
  getline(cin, numberCheck);
  int number = stoi(numberCheck);
  while (((number%2) == 0) && number > 1){
    number /= 2;
  }
  if (number == 1){
    cout << "That Number is a power of 2!" << endl;
  } else {
    cout << "That Number is not a power of 2!" << endl;
  }
}

void checkIfPalindrome(){

  string numberCheck;
  cout << "Type Number to check if palindrome: ";
  getline(cin, numberCheck);
  int number = stoi(numberCheck);
  reverse(begin(numberCheck), end(numberCheck));
  int reverseNumber = stoi(numberCheck);
  if(number == reverseNumber){
    cout << "This number is a palindrome!" << endl;
  } else {
    cout << "This number is not a palindrome!" << endl;
  }
  
}

void printFibonacci(){
  string userInput;
  cout << "How many numbers of the fibonacci sequence would you like?: ";
  getline(cin, userInput);
  int userInt = stoi(userInput);

  
  int a = 0;
  int b = 1;
  int c;
  for(int i = 0; i < userInt; i++){
    if(i < 1){
      c = 1;
    } else {
      c = a+b;
      a = b;
      b = c;
    }
    cout << c << ", ";
  }
  cout << "..." << endl;
}

void findGCD(){

  string astring;
  string bstring;
  int mini;
  int maxi;
  int result;
  
  cout << "Input first number to find GCD: ";
  getline(cin, astring);
  cout << "Input second number: ";
  getline(cin, bstring);
  int a = stoi(astring);
  int b = stoi(bstring);

  for(int i = 1; i <= a && i <=b ; i++){

    if(b % i == 0 && a % i == 0){
      result = i;
    }
  }
  cout << result << " is the greatest common divisor!" << endl;
  
  
}
  
int main(){

  string menu =
    "Welcome to the Number Cruncher\nMenu:\n1. Check if number is power of two\n2. Check if number is a palindrome\n3. Print digits of the fibonacci sequence\n4. Find the greatest common denominator of two numbers\n5. Exit\nYour option: ";

  string userOption;
 startMenu:
  cout << menu;
  getline(cin, userOption);
  int option = stoi(userOption);

  switch(option){
  case 1:
    checkPowerOfTwo();
    break;
  case 2:
    checkIfPalindrome();
    break;
  case 3:
    printFibonacci();
    break;
  case 4:
    findGCD();
    break;
  case 5:
    cout << "Exiting Number Cruncher . . ." << endl;
    return 0;
  default:
    cout << "That is not a valid option try again" << endl;
    sleep(2);
    goto startMenu;
  }
  sleep(2);
  goto startMenu;
}
